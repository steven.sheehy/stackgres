---
title: Service Mesh Integration
url: install/services-mesh-integration
aliases: [ /install/prerequisites/services-mesh-integration ]
weight: 5
description: Details about the different options to integrate StackGres with some service mesh implementations.
---

# Service Mesh Integration

Details about the different options to integrate StackGres with some service mesh implementations.

{{% children style="li" depth="2" description="true" %}}